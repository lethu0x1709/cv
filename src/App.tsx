import React from 'react';
import logo from './avatarcv.jpg';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className='content'>
        <div className='content-left'>
          <img src={logo} className="Avatar" alt="Avatar" />
          <h3>Thông tin cá nhân</h3>
          <div className='information'>
          <div>lethu0x1709@gmail.com</div>
          <div>0326813553</div>
          <div>Bình Chánh, TP.HCM</div>
          </div>
          <h3>Các kỹ năng</h3>
          <ul>
            <li>HTML</li>
            <li>CSS</li>
            <li>JavaScript</li>
            <li>Tiếng Anh (có thể đọc hiểu)</li>
            <li>Photoshop</li>
            <li>Adobe Illustrator</li>
          </ul>
          <h3>Công cụ phát triển</h3>
          <ul>
            <li>Visual Studio Code</li>
          </ul>
        </div>
        <div className='content-right'>
          <h3>Lê Thị Minh Thư</h3>
          <h3>Front-end</h3>
          <h4 style={{border:'1px solid #8a2be2', borderRadius:'30px'}}>Mục tiêu nghề nghiệp</h4>
          <p>Là một sinh viên với các kỹ năng xây dựng giao diện website. Tôi đang tìm
              kiếm cơ hội thực tập để áp dụng kiến thức của mình và mong muốn được học
              hỏi và trau dồi thêm kinh nghiệm từ các chuyên gia trong ngành. Tôi mong
              muốn có cơ hội tham gia vào việc phát triển các ứng dụng web tiên tiến, tạo ra
              giao diện người dùng tuyệt vời và trải nghiệm tốt nhất cho người dùng.</p>
              <h4 style={{border:'1px solid #8a2be2', borderRadius:'30px'}}>Học vấn</h4>
              <h5>Trường Cao đẳng Công thương Việt Nam <span>// 08/2022-nay</span></h5>
              <p>Bằng Cử nhân Công nghệ thông tin (dự kiến tốt nghiệp vào 01/2025)</p>
              <h4 style={{border:'1px solid #8a2be2', borderRadius:'30px'}}>Dự án</h4>
              <div>
                <h5 className='title'>MÔ PHỎNG LẠI WEB THẾ GIỚI DI ĐỘNG<span>// 26/05/2024 - nay</span></h5>
                <a href="https://drive.google.com/drive/folders/17qtY9ChCJ-dcw9thWwxOAPA4meHoRdBq">Link</a>
                <p className='sl'>Số lượng người tham gia: 1(Project luyện kỹ năng)</p>
                <p className='cn'>Công nghệ sử dụng: HTML, CSS, JavaScrip</p>
                <p className='mt'>Sử dụng html, css, javascrip mô phỏng lại giao diện web thế giới di động</p>
              </div>
              <div>
                <h5 className='title'>SHOP ĐIỆN THOẠI ONLINE<span>// 24/03/2024 - 17/04/2024</span></h5>
                <a href="http://shopdt.byethost7.com/">Link</a>
                <p className='sl'>Số lượng người tham gia: 1</p>
                <p className='mt'>Sử dụng Wordpress tạo nên giao diện thân thiện và dễ sử dụng</p>
              </div>
              <div>
                <h5 className='title'>WEB THE BAND<span>// -</span></h5>
                <a href="https://drive.google.com/drive/folders/1XkQTtzl7JzOO2-NQUvuC-e4s6G9Tc7IX">Link</a>
                <p className='sl'>Số lượng người tham gia: 1(Project tự học)</p>
                <p className='cn'>Công nghệ sử dụng: HTML, CSS, JavaScrip</p>
                <p className='mt'>Sử dụng html, css, javascrip mô phỏng lại giao diện web thế giới di động</p>
              </div>
        </div>
      </div>
    </div>
  );
}

export default App;
